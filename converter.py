#!/usr/bin/python3
import PySimpleGUI as gui

converter_values = ['km to mile', 'kg to pound', 'sec to minutes']

layout = [[gui.Input(key='-INPUT-'), gui.Spin(converter_values, key='-UNITS-'), gui.Button("Convert", key='-BUTTON-')],
          [gui.Text("Output", key='-OUTPUT-')]]
window = gui.Window("Converter", layout)

while True:
    event, values = window.read()

    if event == gui.WIN_CLOSED:
        break
    
    if event == '-BUTTON-':
        input_value = values['-INPUT-']
        if input_value.isnumeric():
            match values['-UNITS-']:
                case 'km to mile':
                   output = round(float(input_value) * 0.6214,2)
                   output_string = f'{input_value} km are {output} miles.'
                case 'kg to pound':
                    output = round(float(input_value) * 2.205,2)
                    output_string = f'{input_value} kg are {output} lb.'
                case 'sec to minutes':
                    output = round(float(input_value) /60.0,2)
                    output_string = f'{input_value} sec are {output} minutes.'
            window['-OUTPUT-'].update(output_string)
        else:
            window['-OUTPUT-'].update("Error: not numeric input")      
window.close()
